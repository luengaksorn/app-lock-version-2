import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:convert/convert.dart';
import 'dart:typed_data';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io';
import 'package:loading/loading.dart';
import 'package:loading/indicator/ball_scale_indicator.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/indicator/ball_grid_pulse_indicator.dart';
import 'package:loading/indicator/line_scale_indicator.dart';
import 'package:loading/indicator/ball_scale_multiple_indicator.dart';
import 'package:loading/indicator/line_scale_party_indicator.dart';
import 'package:loading/indicator/ball_beat_indicator.dart';
import 'package:loading/indicator/line_scale_pulse_out_indicator.dart';
import 'package:loading/indicator/ball_spin_fade_loader_indicator.dart';

ProgressDialog progressDialog;

class ScanQRcode extends StatefulWidget {
  final List<BluetoothDevice> devicesList = new List<BluetoothDevice>();
  final Map<Guid, List<int>> readValues = new Map<Guid, List<int>>();

  @override
  _ScanQRcodeState createState() => _ScanQRcodeState();
}

class _ScanQRcodeState extends State<ScanQRcode> {
  bool foundStat = false;
  bool unlockStat = false;
  bool connectStat = false;
  String qrCodeResult = "";
  String state_process = "Unlocking";
  String convert_manufacturerData = "";
  BluetoothDevice _connectedDevice;
  List<BluetoothService> _services;
  FlutterBlue flutterBlue = FlutterBlue.instance;
  List<ScanResult> dta = [];

  void findDevice(device, advertisementData) {
    var manu_data_keys = advertisementData.manufacturerData.keys;

    if (manu_data_keys.single == 22780) {
      _convert_string_Device(advertisementData.manufacturerData);

      if (convert_manufacturerData == qrCodeResult) {
        if (foundStat == false) {
          print("i found This LYS");
          flutterBlue.stopScan();
          foundStat = true;
          return _Connect_Device(device);
        }
      }
    }
  }

  void Start_Scan() async {
    await flutterBlue.startScan(timeout: Duration(seconds: 5));
    // Listen to scan results
    await flutterBlue.scanResults.listen((results) {
      for (ScanResult result in results) {
        print(result.device.name);
        if (result.device.name == 'LYS') {
          print(result.advertisementData.manufacturerData);
          var device = result.device;
          var advertisementData = result.advertisementData;
          print(device.name);
          findDevice(device, advertisementData);
        }
      }
      // print(results.length);
    });

    flutterBlue.stopScan();
    print("---foundStat---");
    print(foundStat);
    if (foundStat == false) {
      Future.delayed(Duration(seconds: 1), () {
        Navigator.of(context).pop();
        SweetAlert.show(context,
            title: "Unlock fail",
            subtitle: "Device Not Found, Please try again.",
            style: SweetAlertStyle.error);
      });
    }
  }

  void _convert_string_Device(data) async {
    print("_convert_string_Device---------->");
    var manufacturerData = Uint8List.fromList(data[22780]);
    var result_anufac_convert = hex.encode(manufacturerData);
    convert_manufacturerData = "fc58" + result_anufac_convert;
  }

  void _Connect_Device(final BluetoothDevice device) async {
    print("_Connect_Device---------->");
    if (connectStat == false) {
      await device.connect();
      connectStat = true;
      _services = await device.discoverServices();
      findServiceDevice(device);
    }
  }

  void findServiceDevice(device) {
    if (unlockStat == false) {
      for (BluetoothService service in _services) {
        for (BluetoothCharacteristic characteristic
            in service.characteristics) {
          if (characteristic.uuid.toString() ==
              '0000ff01-0000-1000-8000-00805f9b34fb') {
            return unlockDevice(characteristic, device);
          }
        }
      }
    }
    //if service not found
    Future.delayed(Duration(seconds: 4), () {
      Navigator.of(context).pop();
    });
  }

  void unlockDevice(BluetoothCharacteristic characteristic, device) {
    print("_Unlock_write_value---------->");
    characteristic.write([
      0x00,
      0x06,
      0xcc,
      0x59,
      0x51,
      0x3c,
      0x4c,
      0xaa,
      0x61,
      0x16,
      0xd3,
      0x4b,
      0xf7,
      0x10,
      0x00,
      0xb1,
      0x2e,
      0xf8,
    ]);
    Future.delayed(Duration(seconds: 2), () {
      print("disconnected");
      device.disconnect();
    });
    unlockStat = true;
    print(unlockStat);

    // Future.delayed(Duration(seconds: 1), () {
    Navigator.of(context).pop();
    SweetAlert.show(context,
        title: "Unlock Successful", style: SweetAlertStyle.success);
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        // Don't show the leading button
        toolbarHeight: 80,
        elevation: 0.5,
        centerTitle: true,
        backgroundColor: Colors.teal,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(bottom: Radius.circular(45)),
        ),
        title: Text(
          'One Smart Access',
          style: TextStyle(
            fontFamily: 'Varela',
            fontSize: 22,
            foreground: Paint()..color = Colors.white,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(12.0),
        margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(20.0),
              margin:
                  const EdgeInsets.only(left: 16, right: 16, top: 0, bottom: 5),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.grey[200],
                      Colors.grey[200],
                      Colors.grey[200]
                    ],
                  ),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25.0),
                      topRight: Radius.circular(25.0),
                      bottomLeft: Radius.circular(25.0),
                      bottomRight: Radius.circular(25.0))),
              child: Center(
                  child: ListTile(
                      title: Text(
                        "Scan QR Code",
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold),
                      ),
                      subtitle: Text(
                        "To setup your device, please scan the QR code on the device.",
                        style: TextStyle(
                            fontFamily: 'Montserrat',
                            color: Colors.black45,
                            fontSize: 14.0,
                            fontWeight: FontWeight.normal),
                      ),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            // padding: EdgeInsets.all(5),
                            child: Icon(
                              Icons.qr_code_scanner,
                              color: Colors.black,
                              size: 45,
                            ),
                          ),
                        ],
                      ))),
            ),
            Container(
              padding: EdgeInsets.only(right: 25, left: 25, top: 0, bottom: 10),
              child: Image(
                image: AssetImage('images/exampleQRcode.png'),
                fit: BoxFit.cover,
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(80, 0, 80, 0),
              child: Row(
                children: [
                  Expanded(
                      child: ButtonTheme(
                          height: 50.0,
                          child: RaisedButton(
                            color: Colors.teal[50],
                            onPressed: () async {
                              String codeSanner =
                                  await BarcodeScanner.scan(); //barcode scnner
                              setState(() {
                                qrCodeResult = codeSanner;
                                unlockStat = false;
                                connectStat = false;
                                foundStat = false;
                              });

                              if (qrCodeResult.toString() != "") {
                                Start_Scan();
                                showAlertDialog(context);
                              }
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                side: BorderSide(color: Colors.teal)),
                            child: Text(
                              "Start Scan",
                              style: TextStyle(
                                color: Colors.teal,
                                fontSize: 18,
                                fontFamily: 'Varela',
                              ),
                            ),
                          )))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    print("showAlertDialog------------------>");
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return Center(
          child: Card(
            margin: EdgeInsets.only(left: 15.0, right: 15.0),
            shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.white70, width: 5),
              borderRadius: BorderRadius.circular(25),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    SizedBox(
                      height: 15.0,
                    ),
                    Text(
                      "One Smart Access",
                      style: TextStyle(
                          fontFamily: 'Montserrat',
                          color: Colors.black,
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(
                      qrCodeResult,
                      style: TextStyle(
                          fontFamily: 'Montserrat',
                          color: Colors.grey,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Image(
                      image: AssetImage('images/box1.png'),
                      fit: BoxFit.cover,
                      width: 150.0,
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Text(
                      state_process + "...",
                      style: TextStyle(
                          fontFamily: 'Montserrat',
                          color: Colors.grey,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold),
                    ),
                    Center(
                      child: Loading(
                          indicator: BallPulseIndicator(),
                          size: 60.0,
                          color: Colors.teal),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
